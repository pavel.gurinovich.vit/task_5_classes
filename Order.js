import ShippingInfo from "./ShoppingInfo";
import OrderDetails from "./OrderDetails";
import User from "./User";
class Order {
    constructor(dateCreated, dateShipped, costumerName, costumerId, status) {
      this._orderId = "";
      this.dateCreated = dateCreated;
      this.dateShipped = dateShipped;
      this.costumerName = costumerName;
      this.costumerId = costumerId;
      this.status = status;
      this._shippingId = "";
    }
    set orderId(value) {
      if (isNaN(value)) {
        throw new Error("Invalid type");
      }
      this._orderId = value;
    }
    get orderId() {
      return this._orderId;
    }
  
    set shippingId(value) {
      if (isNaN(value)) {
        throw new Error("Invalid type");
      }
      this._shippingId = value;
    }
  
    get shippingId() {
      return this._shippingId;
    }
    placeOrder() {}
  }
  const order = new Order("01.06.2021", "01.06.2021", "Solo", "1", "Delivered");
  try {
    order.orderId = "555";
  } catch (e) {
    console.log(e.message);
  }
  try {
    order.shippingId = "asd";
  } catch (e) {
    console.log(e.message);
  }
  console.log(order);
  
