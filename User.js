
class User {
    constructor(userId, loginStatus) {
      this.userId = userId;
      this._password = "";
      this.loginStatus = loginStatus;
      this._registerDate = "";
    }
    set registerDate(value) {
      if (!Date.parse(value)) {
        throw new Error("Invalid date form");
      }
      this._registerDate = value;
    }
    get registerDate() {
      return this._registerDate;
    }
    set password(value) {
      if (value.length < 6) {
        throw new Error("Password is too short");
      }
      this._password = value;
    }
    get password() {
      return this._password;
    }
    veryfyLogin() {}
  }
  
  const user = new User("1", "login");
  try {
    user.password = "2фыв";
  } catch (e) {
    console.log(e.message);
  }
  try {
    user.registerDate = "02.30.2021";
  } catch (e) {
    console.log(e.message);
  }
  console.log(user);
  
  export default User