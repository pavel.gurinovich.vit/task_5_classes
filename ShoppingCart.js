class ShoppingCart {
  constructor(dateAdded) {
    this._cartId = "";
    this._productId = "";
    this._quantity = "";
    this.dateAdded = dateAdded;
  }
  set cartId(value) {
    if (isNaN(value)) {
      throw new Error("Invalid type");
    }
    this._cartId = value;
  }
  get cartId() {
    return this._cartId;
  }
  set productId(value) {
    if (isNaN(value)) {
      throw new Error("Invalid type");
    }
    this._productId = value;
  }
  get productId() {
    return this._productId;
  }
  set quantity(value) {
    if (value < 1 || typeof value !== "number") {
      throw new Error("Invalid quantity");
    }
    this._quantity = value;
  }
  get quantity() {
    return this._quantity;
  }

  addCartItem() {}
  updateQuantity() {}
  viewCartDetails() {}
  checkOut() {}
}

const cartOne = new ShoppingCart("01.06.2021");
try {
  cartOne.cartId = "33ф";
} catch (e) {
  console.log(e.message);
}
try {
  cartOne.productId = "34ф";
} catch (e) {
  console.log(e.message);
}
try {
  cartOne.quantity = "34ф";
} catch (e) {
  console.log(e.message);
}

console.log(cartOne);

export default ShoppingCart;
