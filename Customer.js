import ShoppingCart from "./ShoppingCart";
import Order from "./Order";
class Customer {
    constructor(adress, creditCardInfo, shippingInfo) {
      this._costumerName = "";
      this.adress = adress;
      this._email = "";
      this.creditCardInfo = creditCardInfo;
      this.shippingInfo = shippingInfo;
      this._accountBallance = "";
    }
    set costumerName(value) {
      if (Number(value)) {
        throw new Error("Invalid type");
      }
      this._costumerName = value;
    }
    get costumerName() {
      return this._costumerName;
    }

    set email(value) {
      let check = /\S+@\S+\.\S+/;
      if (!check.test(value)) {
        throw new Error("Invalid email address");
      }
      this._email = value;
    }
    get email() {
      return this._email;
    }
    set accountBallance(value) {
      if (isNaN(value)) {
        throw new Error("Invalid type");
      }
      this._accountBallance = value;
    }
    get accountBallance() {
      return this._accountBallance;
    }
    register() {}
    login() {}
    updateProfile() {}
  }
  const costumer = new Customer(
    "Abu Dhabi",
    "visa platinum",
    "Lufftawaffe"
  );
  try {
    costumer.costumerName = "123";
  } catch (e) {
    console.log(e.message);
  }
  try {
    costumer.accountBallance = "1a23";
  } catch (e) {
    console.log(e.message);
  }
  try {
    costumer.email = "pashagurinovich@gmail.com";
  } catch (e) {
    console.log(e.message);
  }
  console.log(costumer);
  
export default Customer