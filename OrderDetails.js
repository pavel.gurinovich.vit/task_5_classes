
class OrderDetails {
  constructor(orderId, productId, productName, unitCost, subTotal) {
    this.orderId = orderId;
    this.productId = productId;
    this.productName = productName;
    this._quantity = "";
    this.unitCost = unitCost;
    this.subtotal = subTotal;
  }

  set quantity(value) {
    if (value < 1 || typeof value !=="number") {
      throw new Error("Invalid quantity");
    }
    this._quantity = value;
  }
  get quantity() {
    return this._quantity;
  }

  calcPrice() {}
}

const orderDetails = new OrderDetails("1", "1", "book", "47", "98.1");
try {
  orderDetails.quantity = "0";
} catch (e) {
  console.log(e.message);
}
console.log(orderDetails);

export default OrderDetails