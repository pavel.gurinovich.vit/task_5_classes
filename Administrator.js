
import User from "./User";
class Administrator {
    constructor(adminName) {
      this.adminName = adminName;
      this._email = "";
    }
    set email(value) {
      let check = /\S+@\S+\.\S+/;
      if (!check.test(value)) {
        throw new Error("Invalid email address");
      }
      this._email = value;
    }
    get email() {
      return this._email;
    }
    updateCatalog() {}
  }
  const admin = new Administrator("Pasha");
  try {
    admin.email = "pashagurinovich@gmail.com";
  } catch (e) {
    console.log(e.message);
  }
  console.log(admin);