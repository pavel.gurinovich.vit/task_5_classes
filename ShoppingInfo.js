class ShippingInfo {
    constructor(shippingId, shippingCost, shippingRegionId) {
      this.shippingId = shippingId;
      this._shippingType = "";
      this.shippingCost = shippingCost;
      this.shippingRegionId = shippingRegionId;
    }
    set shippingType(value) {
      if (typeof value !== "string") {
        throw new Error("Invalid shipping type");
      }
      this._shippingType = value;
    }
    get _hippingType() {
      return this._shippingType;
    }
    updateShippingInfo() {}
  }
  const shipping = new ShippingInfo("1", "777", "1488");
  try {
    shipping.shippingType = "Plane";
  } catch (e) {
    console.log(e.message);
  }
  console.log(shipping);
  
  export default ShippingInfo